import QtQuick 2.9
import QtQuick.Layouts 1.3
import Lomiri.Content 0.1
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3
import Qt.labs.settings 1.0
import QtWebEngine 1.7
import Morph.Web 0.1

import JadeDiamond 1.0
import PamAuthentication 0.1

// TODO split this into smaller files
Page {
    function parseDomain(url) {
        var domain = url.replace('http://', '').replace('https://', '')
        if (domain.indexOf('/') > -1) {
            domain = domain.substring(0, domain.indexOf('/'));
        }

        //Treat www.ABC.com domains the same as ABC.com
        if (domain.substring(0, 4) == 'www.') {
            domain = domain.replace('www.', '')
        }

        return domain;
    }

    function navigationRequest(url) {
        var ok = false; // Always reject navigation unless its on the whitelist
        console.log('requesting navigation to ' + url);

        var domain = parseDomain(url);
        if (domain == 'localhost') {
            console.log('rejecting navigation to localhost');

            navigator.text = webview.url;
            PopupUtils.open(rejectionComponent, root, {text: i18n.tr('Accessing localhost is not allowed')});
        }
        else if (url.indexOf('http://file://') === 0) { // http gets automatically appened for urls imputted by the user
            console.log('rejecting navigation to file url');

            navigator.text = webview.url;
            PopupUtils.open(rejectionComponent, root, {text: i18n.tr('Accessing a file url is not allowed')});
        }
        else if (isNaN(domain.replace(/\./g, ''))) {
            if (JadeDiamond.isBlacklisted(domain)) {
                console.log('rejected navigation because the request is for a blacklisted domain');

                navigator.text = webview.url;
                PopupUtils.open(rejectionComponent, root, {text: i18n.tr('Access to this site has been blocked')});
            }
            else if (JadeDiamond.isWhitelisted(domain)) {
                console.log('navigation ok because the request is for a whitelisted domain');
                ok = true;
            }
            else {
                // Not in the whitelist or the blacklist
                navigator.text = webview.url;
                console.log('navigation rejected, but asking for permission');
                PopupUtils.open(confirmNavigationComponent, root, {
                    url: url,
                    domain: domain,
                });
            }
        }
        else { //The domain is acutally an ip address, reject this
            console.log('rejected navigation because the request is for an ip address');

            navigator.text = webview.url;
            PopupUtils.open(rejectionComponent, root, {text: i18n.tr('Accessing IP addresses is not allowed')});
        }

        return ok;
    }

    function navigate(url) {
        if (url.substring(0, 7) != 'http://' && url.substring(0, 8) != 'https://') {
            url = 'http://' + url;
        }

        if (navigationRequest(url)) {
            webview.url = url;
        }
    }

    function closeListPage(url) {
        if (url && navigationRequest(url)) {
            webview.url = url;
        }

        pageStack.pop();
    }

    Arguments {
        id: args

        Argument {
            name: 'url'
            help: i18n.tr('Startup url')
            required: false
            valueNames: ['URL']
        }
    }

    Connections {
        target: UriHandler
        onOpened: {
            if (uris.length > 0) {
                console.log('navigating from UriHandler');
                navigate(uris[0]);
            }
        }
    }

    Component.onCompleted: {
        if (args.values.url && args.values.url.match(/^http/)) {
            webview.url = args.values.url;
        }
        else if (Qt.application.arguments && Qt.application.arguments.length > 0) {
            for (var i = 0; i < Qt.application.arguments.length; i++) {
                if (Qt.application.arguments[i].match(/^http/)) {
                    webview.url = Qt.application.arguments[i];
                }
            }
        }
    }

    Settings {
        id: settings
        property string homePage
    }

    header: PageHeader {
        id: header

        contents: RowLayout {
            anchors {
                fill: parent
                topMargin: units.gu(1)
                bottomMargin: units.gu(1)
            }

            spacing: units.gu(1)

            TextField {
                id: navigator

                Layout.fillWidth: true
                inputMethodHints: Qt.ImhUrlCharactersOnly | Qt.ImhNoPredictiveText | Qt.ImhNoAutoUppercase

                onAccepted: {
                    welcome.visible = false;
                    navigate(text);
                }
            }
        }

        trailingActionBar {
            actions: [
                Action {
                    iconName: 'go-next'
                    text: i18n.tr('Forward')
                    enabled: webview.canGoForward
                    onTriggered: {
                        welcome.visible = false;
                        webview.goForward();
                    }
                },

                Action {
                    iconName: 'go-previous'
                    text: i18n.tr('Back')
                    enabled: webview.canGoBack
                    onTriggered: {
                        welcome.visible = false;
                        webview.goBack();
                    }
                },

                Action {
                    iconName: 'reload'
                    text: i18n.tr('Refresh')
                    onTriggered: {
                        welcome.visible = false;
                        webview.reload();
                    }
                },

                Action {
                    iconName: 'go-home'
                    text: i18n.tr('Home')
                    visible: settings.homePage
                    onTriggered: {
                        welcome.visible = false;
                        navigate(settings.homePage);
                    }
                },

                Action {
                    iconName: 'history'
                    text: i18n.tr('History')
                    onTriggered: {
                        welcome.visible = false;
                        var page = pageStack.push(Qt.resolvedUrl('HistoryPage.qml'));
                        page.onUrlSelected.connect(closeListPage);
                    }
                },

                Action {
                    iconName: 'view-list-symbolic'
                    text: i18n.tr('Blacklist')
                    onTriggered: {
                        welcome.visible = false;
                        pageStack.push(Qt.resolvedUrl('BlacklistPage.qml'));
                    }
                },

                Action {
                    iconName: 'view-list-symbolic'
                    text: i18n.tr('Whitelist')
                    onTriggered: {
                        welcome.visible = false;
                        var page = pageStack.push(Qt.resolvedUrl('WhitelistPage.qml'));
                        page.onUrlSelected.connect(closeListPage);
                    }
                },

                Action {
                    iconName: 'stock_website'
                    text: i18n.tr('Welcome')
                    onTriggered: welcome.visible = true
                },

                Action {
                    iconName: 'info'
                    text: i18n.tr('About')
                    onTriggered: {
                        welcome.visible = false;
                        var page = pageStack.push(Qt.resolvedUrl('AboutPage.qml'));
                        page.onUrlSelected.connect(closeListPage);
                    }
                }
            ]
        }
    }

    WebContext {
        id: webcontext
        userAgent: 'Mozilla/5.0 (Linux; Ubuntu 20.04 like Android 9) AppleWebKit/537.36 Chrome/87.0.4280.144 Mobile Safari/537.36'
        offTheRecord: false
    }

    // TODO context menu
    // TODO handle opening things in a "new tab" (just force it into the current view)
    WebView {
        id: webview
        anchors {
            top: header.bottom
            right: parent.right
            left: parent.left
            bottom: parent.bottom
        }

        visible: !welcome.visible

        // Copied from morph: https://github.com/ubports/morph-browser/blob/8d95c2b123c71a9368617f429643c8182685235d/src/app/WebViewImpl.qml
        onJavaScriptDialogRequested: function(request) {
            switch (request.type)
            {
                case JavaScriptDialogRequest.DialogTypeAlert:
                    request.accepted = true;
                    var alertDialog = PopupUtils.open(Qt.resolvedUrl('BrowserDialogs/AlertDialog.qml'));
                    alertDialog.message = request.message;
                    alertDialog.accept.connect(request.dialogAccept);
                    break;

                case JavaScriptDialogRequest.DialogTypeConfirm:
                    request.accepted = true;
                    var confirmDialog = PopupUtils.open(Qt.resolvedUrl('BrowserDialogs/ConfirmDialog.qml'));
                    confirmDialog.message = request.message;
                    confirmDialog.accept.connect(request.dialogAccept);
                    confirmDialog.reject.connect(request.dialogReject);
                    break;

                case JavaScriptDialogRequest.DialogTypePrompt:
                    request.accepted = true;
                    var promptDialog = PopupUtils.open(Qt.resolvedUrl('BrowserDialogs/PromptDialog.qml'));
                    promptDialog.message = request.message;
                    promptDialog.defaultValue = request.defaultText;
                    promptDialog.accept.connect(request.dialogAccept);
                    promptDialog.reject.connect(request.dialogReject);
                    break;

                // did not work with JavaScriptDialogRequest.DialogTypeUnload (the default dialog was shown)
                //case JavaScriptDialogRequest.DialogTypeUnload:
                case 3:
                    request.accepted = true;
                    var beforeUnloadDialog = PopupUtils.open(Qt.resolvedUrl('BrowserDialogs/BeforeUnloadDialog.qml'));
                    beforeUnloadDialog.message = request.message;
                    beforeUnloadDialog.accept.connect(request.dialogAccept);
                    beforeUnloadDialog.reject.connect(request.dialogReject);
                    break;
            }
        }

        onFileDialogRequested: function(request) {
            switch (request.mode)
            {
                case FileDialogRequest.FileModeOpen:
                    request.accepted = true;
                    var fileDialogSingle = PopupUtils.open(Qt.resolvedUrl('BrowserDialogs/ContentPickerDialog.qml'));
                    fileDialogSingle.allowMultipleFiles = false;
                    fileDialogSingle.accept.connect(request.dialogAccept);
                    fileDialogSingle.reject.connect(request.dialogReject);
                    break;

                case FileDialogRequest.FileModeOpenMultiple:
                    request.accepted = true;
                    var fileDialogMultiple = PopupUtils.open(Qt.resolvedUrl('BrowserDialogs/ContentPickerDialog.qml'));
                    fileDialogMultiple.allowMultipleFiles = true;
                    fileDialogMultiple.accept.connect(request.dialogAccept);
                    fileDialogMultiple.reject.connect(request.dialogReject);
                    break;

                case FilealogRequest.FileModeUploadFolder:
                case FileDialogRequest.FileModeSave:
                    request.accepted = false;
                    break;
            }
        }

        onColorDialogRequested: function(request) {
            request.accepted = true;
            var colorDialog = PopupUtils.open(Qt.resolvedUrl('BrowserDialogs/ColorSelectDialog.qml'));
            colorDialog.defaultValue = request.color;
            colorDialog.accept.connect(request.dialogAccept);
            colorDialog.reject.connect(request.dialogReject);
        }

        onAuthenticationDialogRequested: function(request) {
            switch (request.type)
            {
                //case WebEngineAuthenticationDialogRequest.AuthenticationTypeHTTP:
                case 0:
                request.accepted = true;
                var authDialog = PopupUtils.open(Qt.resolvedUrl('BrowserDialogs/HttpAuthenticationDialog.qml'), webview.currentWebview);
                authDialog.host = UrlUtils.extractHost(request.url);
                authDialog.realm = request.realm;
                authDialog.accept.connect(request.dialogAccept);
                authDialog.reject.connect(request.dialogReject);

                break;

                //case WebEngineAuthenticationDialogRequest.AuthenticationTypeProxy:
                case 1:
                request.accepted = false;
                break;
            }
        }

        //TODO dns error page

        property string lastUrl

        context: webcontext
        onUrlChanged: {
            var strUrl = url.toString();
            navigator.text = strUrl;

            // Simple check for access here to avoid redirects to restricted pages
            if (strUrl && !JadeDiamond.isWhitelisted(parseDomain(strUrl))) {
                console.log('Going back to the home page because of a redirect to a non-whitelisted page');
                url = home;
            }

            // TODO track the favicon

            if (strUrl != lastUrl) {
                JadeDiamond.addHistory(strUrl, ''); // TODO figure out how to get the title of the page
            }
            lastUrl = strUrl;
        }

        function navigationRequestedDelegate(request) {
            var url = request.url.toString();

            // TODO this could potentially allow access to other sites via an iframe
            if (request.isMainFrame && !navigationRequest(url)) {
                request.action = WebEngineNavigationRequest.IgnoreRequest;
            }
        }

        Component.onCompleted: {
            var url = settings.homePage;
            if (args.values.url && args.values.url.match(/^http/)) {
                url = args.values.url;
            }
            else if (Qt.application.arguments && Qt.application.arguments.length > 0) {
                for (var i = 0; i < Qt.application.arguments.length; i++) {
                    if (Qt.application.arguments[i].match(/^http/)) {
                        url = Qt.application.arguments[i];
                    }
                }
            }

            if (url) {
                console.log('starting up, navigating to:', url);
                navigate(url);
                navigator.text = url;
            }
        }
    }

    ProgressBar {
        height: units.dp(3)
        anchors {
            left: parent.left
            right: parent.right
            top: header.bottom
        }

        showProgressPercentage: false
        value: (webview.loadProgress / 100)
        visible: (webview.loading && !webview.lastLoadStopped)
    }

    WelcomeOverlay {
        id: welcome
        anchors {
            top: header.bottom
            right: parent.right
            left: parent.left
            bottom: parent.bottom
        }

        Component.onCompleted: {
            visible = !settings.homePage;
        }

        onVisibleChanged: homePage = settings.homePage

        onStart: {
            navigate(url);
            settings.homePage = url;
            visible = false;
        }
    }

    Component {
        id: rejectionComponent

        Dialog {
            id: rejectionDialog

            title: i18n.tr('Not Allowed')

            Button {
                text: i18n.tr('OK')
                onClicked: PopupUtils.close(rejectionDialog)
            }
        }
    }

    Component {
        id: confirmNavigationComponent

        Dialog {
            id: confirmNavigationDialog

            AuthenticationHandler {
                id: authHandler
                serviceName: root.applicationName

                onAuthenticationSucceeded: {
                    if (confirmNavigationDialog.goingToBlacklist) {
                        JadeDiamond.addBlacklist(confirmNavigationDialog.domain);
                    }
                    else {
                        JadeDiamond.addWhitelist(confirmNavigationDialog.domain);
                        webview.url = confirmNavigationDialog.url;
                    }

                    PopupUtils.close(confirmNavigationDialog);
                }

                onAuthenticationAborted: {
                    PopupUtils.close(confirmNavigationDialog);
                }
            }

            property string url
            property string domain
            property bool goingToBlacklist: false

            title: i18n.tr('Access Requested')

            // TRANSLATORS: %DOMAIN% will be automatically replaced with the domain being requested
            text: i18n.tr('Access to %DOMAIN% requested').replace('%DOMAIN%', domain)

            Button {
                text: i18n.tr('Grant Access')
                color: LomiriColors.green
                onClicked: {
                    goingToBlacklist = false;

                    // TRANSLATORS: %DOMAIN% will be automatically replaced with the domain being requested
                    authHandler.authenticate(i18n.tr('Enter your password to grant access to %DOMAIN%').replace('%DOMAIN%', domain));

                    // For debugging on the desktop
                    /*
                    JadeDiamond.addWhitelist(confirmNavigationDialog.domain);
                    webview.url = confirmNavigationDialog.url;
                    PopupUtils.close(confirmNavigationDialog);
                    */
                }
            }

            Button {
                text: i18n.tr('Block Access')
                color: LomiriColors.red
                onClicked: {
                    goingToBlacklist = true;

                    // TRANSLATORS: %DOMAIN% will be automatically replaced with the domain being requested
                    authHandler.authenticate(i18n.tr('Enter your password to block access to %DOMAIN%').replace('%DOMAIN%', domain));

                    // For debugging on the desktop
                    /*
                    JadeDiamond.addBlacklist(confirmNavigationDialog.domain);
                    PopupUtils.close(confirmNavigationDialog);
                    */
                }
            }

            Button {
                text: i18n.tr('Cancel')
                onClicked: PopupUtils.close(confirmNavigationDialog)
            }
        }
    }
}
