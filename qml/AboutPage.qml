import QtQuick 2.4
import QtQuick.Layouts 1.1
import Lomiri.Components 1.3

Page {
    signal urlSelected(string url)

    header: PageHeader {
        id: header

        title: i18n.tr('About')
    }

    Flickable {
        anchors {
            top: header.bottom
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }
        contentHeight: contentColumn.height + units.gu(4)

        ColumnLayout {
            id: contentColumn
            anchors {
                left: parent.left;
                top: parent.top;
                right: parent.right;
                margins: units.gu(2)
            }
            spacing: units.gu(2)

            Label {
                Layout.fillWidth: true

                horizontalAlignment: Label.AlignHCenter
                textSize: Label.XLarge
                text: i18n.tr('Jade Diamond')
            }

            Item {
                Layout.preferredHeight: logo.height
                Layout.fillWidth: true

                Image {
                    id: logo
                    anchors.centerIn: parent
                    width: parent.width - units.gu(10)
                    height: width

                    source: '../assets/logo.svg'
                }
            }

            Label {
                Layout.fillWidth: true

                text: i18n.tr('Icon by: Joan CiberSheep')
                horizontalAlignment: Text.AlignHCenter
                wrapMode: Text.WordWrap
            }

            Label {
                Layout.fillWidth: true

                text: i18n.tr('Special thanks to Amber for her icon idea!')
                horizontalAlignment: Text.AlignHCenter
                wrapMode: Text.WordWrap
            }

            Label {
                Layout.fillWidth: true

                text: i18n.tr('Special thanks to Luna for naming the app!')
                horizontalAlignment: Text.AlignHCenter
                wrapMode: Text.WordWrap
            }

            Label {
                Layout.fillWidth: true

                text: i18n.tr('Thanks to the UBParents Group for suggestions and testing!')
                horizontalAlignment: Text.AlignHCenter
                wrapMode: Text.WordWrap

                MouseArea {
                    anchors.fill: parent
                    onClicked: urlSelected('https://t.me/UBports_Parents')
                }
            }

            Label {
                Layout.fillWidth: true

                text: i18n.tr('A Brian Douglass app, consider donating if you like it and want to see more apps like it!')
                horizontalAlignment: Text.AlignHCenter
                wrapMode: Text.WordWrap
            }

            Button {
                Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter

                text: i18n.tr('Donate')
                color: LomiriColors.orange
                onClicked: urlSelected('https://liberapay.com/bhdouglass')
            }
        }
    }
}
