# JavaScript Dialogs

These are the JavaScript for the app settings page, these are based on the
dialogs found in the [morph-browser project](https://github.com/ubports/morph-browser/)
(licensed under the GPL v3).
